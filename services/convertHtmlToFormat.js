const CloudConvert = require('cloudconvert');
require('dotenv').config();
const cloudconvert = new CloudConvert(process.env.CLOUD_CONVERT_API_KEY);

const convertHtmlToPdf = async (html, title) => {
  let job = await cloudconvert.jobs.create({
    tasks: {
      'import-task-html-to-pdf': {
        operation: 'import/raw',
        file: `${html}`,
        filename: `${title}.html`,
      },
      'task-html-to-pdf': {
        operation: 'convert',
        input_format: 'html',
        output_format: 'pdf',
        engine: 'chrome',
        input: ['import-task-html-to-pdf'],
        zoom: 1,
        margin_top: 20,
        margin_bottom: 20,
        margin_left: 20,
        margin_right: 20,
        print_background: true,
        display_header_footer: false,
        wait_until: 'load',
        wait_time: 0,
        filename: `${title}.pdf`,
        timeout: 5,
      },
      'export-1': {
        operation: 'export/url',
        input: ['task-html-to-pdf'],
        inline: false,
        archive_multiple_files: false,
      },
    },
    tag: 'html2pdfJobbuilder',
  });

  job = await cloudconvert.jobs.wait(job.id);

  const exportTask = job.tasks.filter(
    (task) => task.operation === 'export/url' && task.status === 'finished'
  )[0];
  const file = exportTask.result.files[0];
  return file.url;
};

const convertHtmlToDocx = async (html, title) => {
  let job = await cloudconvert.jobs.create({
    tasks: {
      'import-task-html-to-docx': {
        operation: 'import/raw',
        file: `${html}`,
        filename: `${title}.html`,
      },
      'task-html-to-docx': {
        operation: 'convert',
        input_format: 'html',
        output_format: 'docx',
        engine: 'office',
        input: ['import-task-html-to-docx'],
        embed_images: true,
        filename: `${title}.docx`,
        timeout: 10,
      },
      'export-1': {
        operation: 'export/url',
        input: ['task-html-to-docx'],
        inline: false,
        archive_multiple_files: false,
      },
    },
    tag: 'html2docxJobbuilder',
  });

  job = await cloudconvert.jobs.wait(job.id);
  const exportTask = job.tasks.filter(
    (task) => task.operation === 'export/url' && task.status === 'finished'
  )[0];
  const file = exportTask.result.files[0];
  return file.url;
};

const convertHtmlToMd = async (html, title) => {
  let job = await cloudconvert.jobs.create({
    tasks: {
      'import-task-html-to-md': {
        operation: 'import/raw',
        file: `${html}`,
        filename: `${title}.html`,
      },
      'task-html-to-md': {
        operation: 'convert',
        input_format: 'html',
        output_format: 'md',
        engine: 'pandoc',
        input: ['import-task-html-to-md'],
        filename: `${title}.md`,
        output_markdown_syntax: 'github',
      },
      'export-1': {
        operation: 'export/url',
        input: ['task-html-to-md'],
        inline: false,
        archive_multiple_files: false,
      },
    },
    tag: 'jobbuilder',
  });

  job = await cloudconvert.jobs.wait(job.id);
  const exportTask = job.tasks.filter(
    (task) => task.operation === 'export/url' && task.status === 'finished'
  )[0];
  const file = exportTask.result.files[0];
  return file.url;
};

module.exports = {
  convertHtmlToPdf,
  convertHtmlToDocx,
  convertHtmlToMd,
};
