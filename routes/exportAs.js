const express = require('express');
const router = express.Router();
const exportAs = require('../controllers/exportAs');
const medium = require('../controllers/medium');

router.post('/exportAs', exportAs.exportAs);

router.get('/medium', medium.postToMedium);

module.exports = router;
