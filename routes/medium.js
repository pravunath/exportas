const express = require('express');
const router = express.Router();
const medium = require('../controllers/medium');

router.post('/medium/add-user-using-token', medium.addUserUsingToken);
router.post('/medium/post-to-medium', medium.postToMedium);

module.exports = router;
