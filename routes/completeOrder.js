const express = require('express');
const router = express.Router();

const completeOrder = require('../controllers/completeOrder');

router.get('/completeOrder/:jwt', completeOrder.completeOrder);

module.exports = router;
