// create an express server that listens on port 5000
const bodyParser = require('body-parser');
const express = require('express');
const exportAsRoute = require('./routes/exportAs');
const completeOrderRoute = require('./routes/completeOrder');
const mediumRoute = require('./routes/medium');

const cors = require('cors');
const app = express();
const port = process.env.PORT || 3030;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(completeOrderRoute);
app.use(exportAsRoute);
app.use(mediumRoute);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
