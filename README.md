#### Setting up Node backend

- Run `yarn` to install dependencies
- Run `yarn server` to start the server
- In the frontend, run `yarn start` to start the frontend
- Click on the **Complete Order** button on the Navbar test the `complete order` API

- The data coming back from the API (for a success) is in the following format:

```json
{
  "message": "Order completed",
  "pdfUrl": "https://storage.cloudconvert.com/tasks/fb7d8bb5-53f5-4932-adc5-b1e09c433744/Test.pdf?AWSAccessKeyId=cloudconvert-production&Expires=1666243146&Signature=sDGP5uXsCKJFUwrQSIF9aF8GOZ0%3D&response-content-disposition=attachment%3B%20filename%3D%22Test.pdf%22&response-content-type=application%2Fpdf",
  "docxUrl": "https://storage.cloudconvert.com/tasks/27c18195-7b85-434f-96d7-6fd396c29667/Test.docx?AWSAccessKeyId=cloudconvert-production&Expires=1666243151&Signature=oDKZvbTRksCa%2BiCbCZ5IJjganrA%3D&response-content-disposition=attachment%3B%20filename%3D%22Test.docx%22&response-content-type=application%2Fvnd.openxmlformats-officedocument.wordprocessingml.document"
}
```

- The data coming back from the API (for a failure) is in the following format:

```json
{
  "message": "Unauthorized, credentials not matching"
}
```
