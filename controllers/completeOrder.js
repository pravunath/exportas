const axios = require('axios');
const jwtDecode = require('jwt-decode');
const convertHtmlToFormat = require('../services/convertHtmlToFormat');

const convertContentToHtml = (title, content) => {
  const fullHtml = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
    </head>
    <body>
      <h1>${title}</h1>
      <div>
        ${content}
      </div>
    </body>
    </html>
    `;
  return fullHtml;
};

exports.completeOrder = async (req, res) => {
  // extract the dynamic part of the url
  const jwt = req.params.jwt;
  // const jwt = req.headers.authorization.split(' ')[1];
  try {
    const decodedToken = jwtDecode(jwt);
    if (decodedToken.issuer === process.env.ISSUER_URL) {
      const encodedArray = JSON.stringify(decodedToken.documentIds);
      const response = await axios.get(
        `https://writerswork.projectreview.co/api/documents/documents-multiple/${encodedArray}`
      );

      const genereatedUrls = await Promise.all(
        response.data.data.map(async (document) => {
          const fullHtml = convertContentToHtml(
            document.title,
            document.content
          );
          const pdfUrl = await convertHtmlToFormat.convertHtmlToPdf(
            fullHtml,
            document.title
          );

          const docxUrl = await convertHtmlToFormat.convertHtmlToDocx(
            fullHtml,
            document.title
          );

          return {
            title: document.title,
            pdfUrl,
            docxUrl,
          };
        })
      );

      res.send({
        data: genereatedUrls,
        message: 'Order completed successfully',
      });
    } else {
      // send a 401 error

      res.status(401).send({
        message: 'Unauthorized, credentials not matching',
      });
    }
  } catch (error) {
    console.log(error);
  }
};
