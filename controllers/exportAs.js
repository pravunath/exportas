const convertHtmlToFormat = require('../services/convertHtmlToFormat');
const convertToHtml = (title, content) => {
  const fullHtml = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
    </head>
    <style>
    * {
      font-family: 'Roboto', sans-serif;
    }

    a {
      color: #1de9b6;
    }
    </style>
    <body>
      <h1>${title}</h1>
      <div>
        ${content}
      </div>
    </body>
    </html>
    `;
  return fullHtml;
};

exports.exportAs = async (req, res) => {
  const { title, content, format } = req.body.data;
  const fullHtml = convertToHtml(title, content);
  try {
    if (format === 'pdf') {
      const pdfUrl = await convertHtmlToFormat.convertHtmlToPdf(
        fullHtml,
        title
      );
      res.send(pdfUrl);
    } else if (format === 'docx') {
      const docxUrl = await convertHtmlToFormat.convertHtmlToDocx(
        fullHtml,
        title
      );
      res.send(docxUrl);
    } else if (format === 'md') {
      const mdUrl = await convertHtmlToFormat.convertHtmlToMd(fullHtml, title);
      res.send(mdUrl);
    }
  } catch (error) {
    console.log(error);
  }
};
