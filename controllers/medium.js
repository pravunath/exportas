const { default: axios } = require('axios');

const addUserUsingToken = async (req, res) => {
  const { integrationToken } = req.body;
  try {
    const response = await axios.get(`https://api.medium.com/v1/me`, {
      headers: {
        Authorization: `Bearer ${integrationToken}`,
      },
    });

    return res.send({ data: response.data.data });
  } catch (err) {
    res.status(err.response.status).send(err.response.statusText);
  }
};

const postToMedium = async (req, res) => {
  const { mediumId, integrationToken, ...rest } = req.body;
  try {
    const response = await axios.post(
      `https://api.medium.com/v1/users/${mediumId}/posts`,
      rest,
      {
        headers: {
          Authorization: `Bearer ${integrationToken}`,
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      }
    );

    return res.send({ data: response.data });
  } catch (err) {
    res.status(err.response.status).send(err.response.statusText);
  }
};

module.exports = {
  postToMedium,
  addUserUsingToken,
};
